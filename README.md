# Brightwheel Email Provider

## Overview

Brightwheel uses internal and third party API’s extensively throughout our platform to process and store data, as well as manage communication to end users. The goal of this exercise is to build and consume an internal API and display the data according to the specifications outlined below. This exercise is meant to assess full stack coding ability, code quality, use of of object oriented principles and your aesthetic.

In order to prevent downtime during an email service provider outage, you’re tasked with creating a service that provides an abstraction between two different email service providers. This way, if one of the services goes down, you can quickly failover to a different provider without affecting your customers.

## Prerequities

* Ruby (~> 2.6.3)
* RubyGems (~> 3.0.3)

## Install

```bash
git clone git@github.com:jramos/brightwheel-email-provider.git
cd brightwheel-email-provider
rvm use
gem install bundler
bundle install
cp .env.example .env
```

## Running

```bash
bundle exec rails server
```

## Testing

```bash
bundle exec rspec
```

## Specifications

An HTTP service that accepts POST requests with JSON data to a ‘/email’ endpoint with the following parameters:

* `to` - email address to send to
* `to_name` - name to accompany the email
* `from` - email address in the from and reply fields
* `from_name` - name to accompany the from/reply emails
* `subject` - subject line of the email
* `body` - HTML body of the email

### Example Request Payload

```json
{
  "to": "fake@example.com",
  "to_name": "Mr. Fake",
  "from": "noreply@mybrightwheel.com",
  "from_name": "Brightwheel",
  "subject": "A Message from Brightwheel",
  "body": "<h1>Your Bill</h1><p>$10</p>"
}
```

The service does a bit of data processing on the request:

* Validates input fields (NOTE: all fields are required).
* Convert the `body` HTML to a plain text version to send along to the email provider.

Once the data has been processed and meets the validation requirements, the email is sent by making an HTTP request to one of the following services:

* [Mailgun](www.mailgun.com)
  * [Documentation](http://documentation.mailgun.com/quickstart.html#sending‐messages)

* [Mandrill](www.mandrillapp.com)
  * [Documentation](https://mandrillapp.com/api/docs/messages.JSON.html#method-send)

* [Sendgrid](www.sendgrid.com)
  * [Documentation](https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/index.html)

All services are free to try and are pretty painless to sign up for, so please register your own test accounts on each and configure `.env` appropriately.

The service sends emails using Mailgun by default, but a simple configuration change and re-deploy of the service will switch it over to another provider. This is accomplished by changing `EMAIL_PROVIDER` in `.env` to `mailgun`, `mandrill` or `sendgrid` and providing the API key, base URL and user via environment variables. See `.env.example` for an example.

## Implementation

This implementation does not use the client libraries provided by Mailgun, Mandrill or Sendgrid. It is making simple `POST` requests using `rest-client`.

This exercise has been organized, designed, documented and tested as if it were going into production.

## TODO

* Fix broken mandrill test
* Add authentication (JWT or Oauth2)
* Use [`vcr`](https://relishapp.com/vcr/vcr/v/1-5-0/docs/test-frameworks/usage-with-rspec) to prevent making actual requests
* Consider using SMTP to make integrations easier
