class EmailController < ApplicationController
  def create
    email = Email.new(email_params)

    begin
      if email.valid? && email.send_email
        render :json => {
          :response => {
            :code => 200,
            :status => "ok"
          }
        }
      else
        raise StandardError.new(email.errors)
      end
    rescue StandardError => err
      render :json => {
        :errors => err.full_message
      }
    end
  end

  private

  def email_params
    params.require(:email).permit(:to, :to_name, :from, :from_name, :subject, :body)
  end
end
