require 'rest-client'

class MandrillService
  def initialize(api_key: nil, base_url: nil, user: nil)
    @api_key = api_key || ENV['MANDRILL_API_KEY']
    @base_url = base_url || ENV['MANDRILL_BASE_URL']
    @user = user || ENV["MANDRILL_USER"]
  end

  def send_email(email)
    RestClient.post(@base_url, json_body(email))
  end

  private

  def json_body(email)
    {
      "key": "#{@api_key}",
      "async": false,
      "message": {
        "text": email.plain_text_body,
        "subject": email.subject,
        "from_email": email.from,
        "from_name": email.from_name,
        "to": [{
          "email": email.to,
          "name": email.to_name,
          "type": "to"
        }],
        "headers": {
          "Reply-To": email.from,
        },
      }
    }.to_json
  end
end
