require 'rest-client'

class SendgridService
  def initialize(api_key: nil, base_url: nil, user: nil)
    @api_key = api_key || ENV['SENDGRID_API_KEY']
    @base_url = base_url || ENV['SENDGRID_BASE_URL']
    @user = user || ENV["SENDGRID_USER"]
  end

  def send_email(email)
    RestClient.post(@base_url, json_body(email), headers)
  end

  private

  def headers
    {
      Authorization: "Bearer #{@api_key}",
      content_type: "application/json"
    }
  end

  def json_body(email)
    {
      "personalizations": [{
        "to": [{
          "email": email.to,
          "name": email.to_name
        }],
        "subject": email.subject
      }],
      "from": {
        "email": email.from,
        "name": email.from_name
      },
      "reply_to": {
        "email": email.to,
        "name": email.to_name
      },
      "subject": email.subject,
      "content": [{
        "type": "text/plain",
        "value": email.plain_text_body
      }]
    }.to_json
  end
end
