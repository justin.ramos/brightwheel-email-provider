require 'rest-client'

class MailgunService
  def initialize(api_key: nil, base_url: nil, user: nil)
    @api_key = api_key || ENV['MAILGUN_API_KEY']
    @base_url = base_url || ENV['MAILGUN_BASE_URL']
    @user = user || ENV['MAILGUN_USER']
  end

  def send_email(email)
    RestClient.post(
      "https://#{@user}:#{@api_key}@#{@base_url}/messages",
      json_body(email),
    )
  end

  private

  def json_body(email)
    {
      :from => email.from,
      :to => email.to,
      :subject => email.subject,
      :text => email.plain_text_body,
      :'o:test-mode' => ENV['RAILS_ENV'] != 'production'
    }
  end
end
