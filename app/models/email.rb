require 'uri'
require 'email_providers/mailgun_service'
require 'email_providers/mandrill_service'
require 'email_providers/sendgrid_service'

class Email
  include ActiveModel::Model

  attr_accessor :to, :to_name, :from, :from_name, :subject, :body

  validates_presence_of :to, :to_name, :from, :from_name, :subject, :body
  validates :to, :from, format: { with: URI::MailTo::EMAIL_REGEXP }

  def self.providers
    ["mailgun", "mandrill", "sendgrid"]
  end

  def persisted?
    false
  end

  def plain_text_body
    ActionController::Base.helpers.strip_tags(self.body)
  end

  def send_email
    provider.send_email(self)
  end

  private

    def provider
      @provider ||= case ENV["EMAIL_PROVIDER"]
        when "mailgun"
          MailgunService.new
        when "mandrill"
          MandrillService.new
        when "sendgrid"
          SendgridService.new
        else
          throw StandardError.new(
            "invalid EMAIL_PROVIDER #{ENV["EMAIL_PROVIDER"]}"
          )
      end
    end
end
