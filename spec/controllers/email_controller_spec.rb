require "rails_helper"

describe EmailController, type: :controller do
  let(:valid_params) do
    {
      :to => "justin.ramos@gmail.com",
      :to_name => "Justin Ramos",
      :from => "noreply@mybrightwheel.com",
      :from_name => "Brightwheel",
      :subject => "A Message from Brightwheel",
      :body => "<h1>Your Bill</h1><p>$10</p>"
    }
  end

  Email.providers.each do |provider|
    describe "#{provider}" do
      before do
        ENV['EMAIL_PROVIDER'] = provider
      end

      describe "POST #create" do
        context "with correct params" do
          it "returns 200" do
            post :create, params: { email: valid_params }
            body = JSON.parse(response.parsed_body)
            expect(body['errors']).to eq(nil)
            expect(body['response']['status']).to eq("ok")
          end
        end

        context "when params are missing" do
          it "returns an error message" do
            post :create, params: { email: { body: "invalid params" } }
            body = JSON.parse(response.parsed_body)
            expect(body['errors']).to_not be_empty
          end
        end
      end
    end
  end
end
